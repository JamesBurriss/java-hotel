/**
 * Beds situated in a room object within a hotel
 *
 * @author James Burriss
 *
 * @version 1.0, $Date: 12/12/2017
 */
abstract class Bed {
    private Room room;
    private int occupancy;

    Bed(Room room) {
        this.room = room;
    }

    public int getOccupancy() {
        return this.occupancy;
    }

    public void setOccupancy(int occupancy) {
        this.occupancy = occupancy;
    }
}
