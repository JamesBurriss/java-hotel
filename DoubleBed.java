/**
 * Abstract parent class to SingleBed and DoubleBed
 *
 * @author James Burriss
 *
 * @version 1.0, $Date: 12/12/2017
 */
class DoubleBed extends Bed {
    DoubleBed(Room room) {
        super(room);

        final int occupancy = 2; // Double bed can support 2
        super.setOccupancy(occupancy);
    }
}