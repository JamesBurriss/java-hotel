import java.util.ArrayList;
import java.util.List;

/**
 * Abstract parent class to SingleBed and DoubleBed
 *
 * @author James Burriss
 *
 * @version 1.0, $Date: 11/12/2017
 */
class Hotel {
    private String hotelName;
    private List<Room> roomsList;

    Hotel() {
        this.roomsList = new ArrayList<>();
    }

    public String getHotelName() {
        return hotelName;
    }

    public void setHotelName(String hotelName) {
        this.hotelName = hotelName;
    }

    public List<Room> getRoomsList() {
        return roomsList;
    }

    /**
     * Adds a room the room list
     *
     * @param r Room object r
     */
    public void addRoom(Room r) {
        this.roomsList.add(r);
    }

    /**
     * Checks if hotel has vacancies using room list
     *
     * @return Boolean - Whether there is vacancies
     */
    public Boolean hasVacancies() {
        for (Room room : this.getRoomsList()) {
            if (!room.isBooked()) {
                return true;
            }
        }

        return false;
    }
}