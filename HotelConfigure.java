import java.util.ArrayList;
import java.util.Scanner;
import java.util.InputMismatchException;

/**
 * HotelConfigure s is used to create an instance of the Hotel object
 * With user input changing the objects attributes
 *
 * @author James Burriss
 *
 * @version 1.0, $Date: 12/12/2017
 */
public class HotelConfigure {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        Hotel hotel = new Hotel();

        generateHotelName(hotel);
        generateRooms(hotel);

        new HotelReport(hotel); // Uses HotelReport to print hotel information
    }

    /**
     * Creates the hotel objects name form user input
     *
     * @param hotel The hotel we're renaming
     */
    private static void generateHotelName(Hotel hotel) {
        System.out.println("What is the hotel name?");

        try {
            hotel.setHotelName(scanner.nextLine());
        } catch (IllegalArgumentException e) {
            System.out.println("Please enter a valid hotel name");
            generateHotelName(hotel);
        }
    }

    /**
     * Creates a user desired number of rooms within the hotel
     * It also asks sets whether a room is booked and calls the generateBeds method
     *
     * @param hotel Hotel that the room is in
     */
    private static void generateRooms(Hotel hotel) {
        System.out.println("How many rooms are in the '" + hotel.getHotelName() + "' hotel?");
        try {
            int numOfRooms = scanner.nextInt();

            validateNonNegativeInteger(numOfRooms);

            for (int i = 0; i < numOfRooms; i++) {
                Room room = new Room(hotel);
                hotel.addRoom(room);

                generateBookedRooms(room);
                generateBeds(room);
            }
        } catch (InputMismatchException e) {
            System.out.println("Please enter a non-negative integer");
            scanner.next();
            generateRooms(hotel);
        }
    }

    /**
     * Sets rooms to be either booked or not
     *
     * @param room The Room which is booked or not
     */
    private static void generateBookedRooms(Room room) {
        System.out.println("\nIs Room #" + room.getRoomNumber() + " booked? ('yes' or 'no')");
        String isBooked = scanner.next();

        if (isBooked.equals("yes") || isBooked.equals("no")) {
            room.setIsBooked(isBooked.equals("yes"));
        } else {
            System.out.println("Please enter 'yes' or 'no' as an input");
            generateBookedRooms(room);
        }
    }

    /**
     * Creates beds within a room
     *
     * @param room The Room in which the bed is being created
     */
    private static void generateBeds(Room room) {
        System.out.println("How many beds in Room #" + room.getRoomNumber());

        try {
            int numOfBeds = scanner.nextInt();

            validateNonNegativeInteger(numOfBeds);

            generateBedType(room, "single");
            generateBedType(room, "double");

            if (room.getNumberOfBeds() != numOfBeds) {
                System.out.println("Number of single/double beds entered doesn't match total number of beds defined");
                room.setBedList(new ArrayList<>());
                generateBeds(room);
            }
        } catch (InputMismatchException e) {
            System.out.println("Please enter a non-negative integer");
            scanner.next();
            generateBeds(room);
        }
    }

    /**
     * Creates bed objects of opposing bed types
     *
     * @param room Room that the bed is in
     * @param bedType Can be either "single" or "double"
     */
    private static void generateBedType(Room room, String bedType) {
        System.out.println("How many " + bedType + " beds in room #" + room.getRoomNumber());

        try {
            int bedCount = scanner.nextInt();

            validateNonNegativeInteger(bedCount);

            for (int i = 0; i < bedCount; i++) {
                Bed bed = bedType.equals("single") ? new SingleBed(room) : new DoubleBed(room);
                room.addBed(bed);
            }
        } catch (InputMismatchException e) {
            System.out.println("Please enter non-negative integer");
            scanner.next();
            generateBedType(room, bedType);
        }
    }

    /**
     * Validates user input, throws exception error with invalid input
     *
     * @param number Number to validate
     * @throws InputMismatchException When number is negative
     */
    private static void validateNonNegativeInteger(int number) throws InputMismatchException {
        if (number < 0) {
            throw new InputMismatchException();
        }
    }
}