import java.util.ArrayList;

/**
 * HotelReport produces a short, textual report describing the name of the hotel, the number of rooms
 * and for each room, lists the number and size of the beds.
 *
 * @author James Burriss
 *
 * @version 1.0, $Date: 12/12/2017
 */
class HotelReport {
    private Hotel hotel;

    /**
     * Creates a HotelReport object
     * @param hotel The Hotel used for the report
     */
    HotelReport(Hotel hotel) {
        this.hotel = hotel;

        showHotelName();
        showOccupancy();
        showVacancy();
        showRooms();
    }

    /**
     * Prints hotel name to console
     */
    private void showHotelName() {
        System.out.println("\nHotel Name: " + this.hotel.getHotelName());
    }

    /**
     * Prints whether the hotel has any availability
     */
    private void showVacancy(){
        System.out.println("Is there any vacancies? " + (this.hotel.hasVacancies() ? "Yes" : "No"));
    }

    /**
     * Prints the maximum occupancy of the hotel by counting number of beds
     */
    private void showOccupancy() {
        int capacity = 0;

        for (Room room : this.hotel.getRoomsList()) {
            ArrayList<Bed> bedList = room.getBedList();
            for (Bed bed : bedList) {
                capacity += bed.getOccupancy();
            }
        }

        System.out.println("Maximum Occupancy: " + capacity); // +2 for double, +1 for single
    }

    /**
     * Prints information for every room. For each room ir describes the total number of beds,
     * the number of double and single beds and whether the room is booked
     *
     */
    private void showRooms() {
        int roomCount = this.hotel.getRoomsList().size();

        for (int i = 0; i < roomCount; i++) {
            Room room = this.hotel.getRoomsList().get(i);

            System.out.println("\nInformation for Room #" + room.getRoomNumber());

            ArrayList<Bed> bedList = room.getBedList();
            System.out.println("Bed Count: " + room.getNumberOfBeds());

            int countSingle = 0;
            int countDouble = 0;

            for (Bed bed : bedList) {
                if (bed instanceof SingleBed) {
                    countSingle = countSingle + 1;
                } else {
                    countDouble = countDouble + 1;
                }
            }

            System.out.println("Number of Single Beds: " + countSingle);
            System.out.println("Number of Double Beds: " + countDouble);
            System.out.println("Is this room booked? " + (room.isBooked() ? "Yes" : "No"));
        }
    }
}