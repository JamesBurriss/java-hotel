/**
 * HotelTest creates objects describing one hotel, with several rooms, each with one or more beds.
 *
 * @author James Burriss
 *
 * @version 1.0, $Date: 13/12/2017
 */
public class HotelTest {
    public static void main(String[] args) {
        Hotel hotel = new Hotel();

        generateHotelName(hotel);
        generateRooms(hotel);

        new HotelReport(hotel);
    }

    /**
     * Creates a hotel name attribute
     *
     * @param hotel The hotel we're naming
     */
    private static void generateHotelName(Hotel hotel) {
        hotel.setHotelName("test hotel");
    }

    /**
     * Creates a random number of beds between 1 and 3
     *
     * @return Randomly generated number of beds
     */
    private static int bedNumberGenerator() {
        return (int) Math.round(Math.random() * 3 + 1);
    }

    /**
     * Creates a random number of rooms between 1 and 5
     *
     * @return Randomly generated number of rooms
     */
    private static int roomNumberGenerator() {
        return (int) Math.round(Math.random() * 5 + 1);
    }

    /**
     * Creates new room objects using the random room generator
     * It also creates bed objects within each room object
     *
     * @param hotel Hotel the rooms are in
     */
    private static void generateRooms(Hotel hotel) {
        int numOfRooms = roomNumberGenerator();

        for (int i = 0; i < numOfRooms; i++) {
            Room room = new Room(hotel);
            hotel.addRoom(room);
            room.setIsBooked(Math.random() < 0.5);

            int numOfSingle = bedNumberGenerator();

            for (int j = 0; j < numOfSingle; j++) {
                Bed bed = new SingleBed(room);
                room.addBed(bed);
            }

            int numOfDouble = bedNumberGenerator();

            for (int k = 0; k < numOfDouble; k++) {
                Bed bed = new DoubleBed(room);
                room.addBed(bed);
            }
        }
    }
}