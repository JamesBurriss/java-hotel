import java.util.ArrayList;

/**
 * A room is situated within a hotel a contains bed objects
 *
 * @author James Burriss
 *
 * @version 1.0, $Date: 12/11/2017
 */
class Room {
    private ArrayList<Bed> bedList;
    private Hotel hotel;
    private Boolean isBooked = false;
    private int roomNumber;

    /**
     *
     * @param hotel
     */
    Room(Hotel hotel) {
        this.bedList = new ArrayList<>();
        this.hotel = hotel;
        this.roomNumber = hotel.getRoomsList().size() + 1;
    }

    /**
     * Adds bed to the bed list
     *
     * @param b Bed object b
     */
    public void addBed(Bed b) {
        this.bedList.add(b);
    }

    public int getNumberOfBeds() {
        return this.bedList.size();
    }

    public ArrayList<Bed> getBedList() {
        return this.bedList;
    }

    public void setBedList(ArrayList<Bed> bedList) {
        this.bedList = bedList;
    }

    public void setIsBooked(Boolean isBooked) {
        this.isBooked = isBooked;
    }

    public Boolean isBooked() {
        return this.isBooked;
    }

    public int getRoomNumber() {
        return this.roomNumber;
    }
}