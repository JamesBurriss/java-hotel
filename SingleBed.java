/**
 * Abstract parent class to SingleBed and DoubleBed
 *
 * @author James Burriss
 *
 * @version 1.0, $Date: 12/12/2017
 */
class SingleBed extends Bed {
    SingleBed(Room room) {
        super(room);

        final int occupancy = 1; // Single bed can support 1
        super.setOccupancy(occupancy);
    }
}
